var supertest = require('supertest')
var app = require('../index.js')

var server = supertest.agent('http://localhost:1337')

describe('GET /', () => {
    it('should return 200', (done) => {
        server.get('/').expect(200, done)
    })
})