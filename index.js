var express = require('express'),
    app     = express()

app.get('/', (req, res) => {
    console.log('a new hello request')
    res.send('hello')
})

app.listen(1337, () => {
    console.log('app is running...')
})